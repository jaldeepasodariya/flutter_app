import 'package:flutter_app/model/MdlAddress.dart';

class MdlUsers {
  final int id;
  final String name;
  final String email;
  final MdlAddress address;

  MdlUsers({this.id, this.name, this.email, this.address});

  factory MdlUsers.fromJson(Map<String, dynamic> json) {
    return MdlUsers(
      id: json['id'] as int,
      name: json['name'] as String,
      email: json['email'] as String,
      address: MdlAddress.fromJson(json['address']),
    );
  }
}