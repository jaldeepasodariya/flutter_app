import 'dart:convert';
import 'package:flutter_app/config/apiConfig.dart';

class MdlLoginReq {
  String email;
}

Map<String, String> requestJsonMap(String emailVal) {

  Map<String, String> generateJsonMap = {

    REQUEST_KEY: json.encode({
      "function": FUNCTION_EMAIL_LOGIN,
      "email": emailVal
    })
  };

  return generateJsonMap;
}

// A function that will convert a response body into a MdlLogin
MdlLoginRes mdlLoginFromJson(String responseBody) {
  final parsed = json.decode(responseBody);
  return MdlLoginRes.fromJson(parsed);
}

class MdlLoginRes {
  final String success;
  final String message;
  final String email;
  final String drId;

  MdlLoginRes({this.success, this.message, this.email, this.drId});

  factory MdlLoginRes.fromJson(Map<String, dynamic> json) => new MdlLoginRes(
    success: json['success'] as String,
    message: json['message'] as String,
    email: json['email'] as String,
    drId: json['dr_id'] as String,
  );
}