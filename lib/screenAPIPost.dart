import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/model/MdlLogin.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:validate/validate.dart';
import 'package:flutter_app/config/apiConfig.dart';

class ScreenAPIPost extends StatefulWidget {
  @override
  _ScreenAPIPostState createState() => _ScreenAPIPostState();
}

class _ScreenAPIPostState extends State<ScreenAPIPost> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('POST: Login - Email'),
      ),
      body: Column(
        children: <Widget>[
          ScreenUI(),
        ],
      ),
    );
  }
}

class ScreenUI extends StatefulWidget {
  @override
  _ScreenUIState createState() => _ScreenUIState();
}

class _ScreenUIState extends State<ScreenUI> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool isLoading = false;
  MdlLoginReq _objMdlLoginReq = new MdlLoginReq();
  MdlLoginRes _objMdlLoginRes;

  String _validateEmail(String emailVal) {
    try {
      Validate.isEmail(emailVal);
    } catch (e) {
      return 'Please enter valid Email';
    }
    return null;
  }

  void checkAllValidation() {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      fetchLogin();
      setState(() {
        isLoading = true; // Show ProgressIndicator
      });
    }
  }

  Future<Null> fetchLogin() async {

    final response = await http.Client().post(Uri.parse(API_ROOT_URL),
        headers: {"Accept": "application/json"},
        body: requestJsonMap(_objMdlLoginReq.email),
        encoding: Encoding.getByName("utf-8"));

    setState(() {
      isLoading = false; // Show ProgressIndicator
      _objMdlLoginRes = mdlLoginFromJson(response.body);
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget widgetFieldsUI() {
      return Form(
          key: this._formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Padding(padding: EdgeInsets.only(top: 16.0)),
              new Card(
                margin: new EdgeInsets.symmetric(horizontal: 24.0),
                child: new Column(
                  children: <Widget>[
                    new Padding(padding: EdgeInsets.only(top: 16.0)),
                    new Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              hintText: 'Enter your email', labelText: 'Email'),
                          validator: this._validateEmail,
                          onSaved: (String value) {
                            this._objMdlLoginReq.email = value;
                          }),
                    ),
                    new Padding(padding: EdgeInsets.only(top: 16.0)),
                    Container(
                      child: MaterialButton(
                        padding: EdgeInsets.symmetric(horizontal: 56.0),
                        textColor: Colors.white,
                        color: Colors.blue,
                        child: Text('Submit'),
                        onPressed: this.checkAllValidation,
                      ),
                    ),
                    new Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  ],
                ),
              ),
            ],
          ));
    }

    Widget widgetFutureAPIResult() {
      return FutureBuilder<MdlLoginRes>(builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);

        return !isLoading && _objMdlLoginRes != null
            ? APIResult(
                resultMdlLogin: _objMdlLoginRes,
              )
            : Container();
      });
    }

    Widget widgetProgressIndicator =
    isLoading ? Center(child: CircularProgressIndicator()) : Container();

    return Container(
      child: Column(
        children: <Widget>[
          widgetFieldsUI(),
          widgetFutureAPIResult(),
          widgetProgressIndicator
        ],
      ),
    );
  }
}

class APIResult extends StatelessWidget {
  final MdlLoginRes resultMdlLogin;
  final String assetConfirmation = 'assets/undraw_confirmation.svg';
  final String assetError = 'assets/undraw_feeling_blue.svg';

  APIResult({Key key, this.resultMdlLogin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        new Card(
          margin: new EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
          child: new Column(
            children: <Widget>[
              new Padding(padding: EdgeInsets.only(top: 16.0)),
              new SvgPicture.asset(
                resultMdlLogin.success == 'true'
                    ? assetConfirmation
                    : assetError,
                height: 150,
                width: 150,
              ),
              new Padding(padding: EdgeInsets.only(top: 16.0)),
              new Text(
                resultMdlLogin.message,
                style: TextStyle(fontSize: 24.0),
              ),
              new Padding(padding: EdgeInsets.only(bottom: 16.0)),
            ],
          ),
        ),
      ],
    );
  }
}
